# pila
Concatenative stack-based language. (written in C99) [not turing complete yet]

```forth
"Hello world" .
```

## Goal
- Keeping it small and portable
- No floating point
- A little of syntactic sugar (so strings literals look like you would expect, for example)

## TODO
- Work in removing hard dependency on libc
- Native code generation?
- Implement more of Pila in itself
- Proper error handling
- Case insensitiveness
- (!!!) Implement threaded execution properly
- FFI
- Being able to be run bare-metal?
