#include <stdint.h>
#include <stddef.h>
#include <stdarg.h>

#define nil ((void *)0)
#define false ((bool)0)
#define true ((bool)1)
/* Length of an array */
#define countof(arr) (isize)(sizeof(arr) / sizeof(*(arr)))
/* Length of string literal */
#define lengthof(s) (countof(s) - 1)
#define Sl(s) ((Str){ s, (isize)lengthof(s) })
#define Str_from_c(s) ((Str){ s, (isize)(s != nil ? strlen(s) : 0) })
#define Str_empty(str) ((str).s == nil || (str).len == 0)

#ifndef NDEBUG
#	ifdef __GNUC__
#		define Assert(pred) if (!(pred)) {__builtin_trap();}
#	else
#		define Assert(pred) if (!(pred)) {abort();}
#	endif
#else
#	define Assert(pred)
#endif

typedef uint8_t u8;
typedef uint64_t u64;
typedef ptrdiff_t isize;
typedef u8 bool;
